<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Patienttest;
use Illuminate\Support\Facades\Input;
use Auth;
use Carbon\Carbon;

class TestsaveDocController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


     public function store(Request $request)
     {

          $appointment=$request->appointment_id;
          $test_id=$request->test_id;
          $docnote=$request->docnote;

      $pttids= Patienttest::where('appointment_id',$appointment)
       ->first();

          if (is_null($pttids)) {
     $PatientTest = Patienttest ::create([
       'appointment_id' => $appointment,
       'test_status' => 0,
     ]);
         $ptid = $PatientTest->id;
          } else {
          // Already test exist - just get the id
           $ptid =$pttids->id;
           DB::table('patient_test')->where('id', $ptid)
           ->update(['test_status' => 0,]);
          }
          DB::table('appointments')->where('id', $appointment)
          ->update(['p_status' => 11,]);

          // Inserting  tests
     $patient_td = DB::table('patient_test_details')->insertGetId([
                       'patient_test_id' => $ptid,
                       'appointment_id' => $appointment,
                       'tests_reccommended' => $test_id,
                       'done' => 0,
                    ]);
        // Inserting patientNotes tests
     if ($docnote) {
     $patientNotes = DB::table('patientNotes')->insert([
         'appointment_id' => $appointment,
         'note' => $docnote,
         'target' => 'Test',
         'ptd_id' => $patient_td,
          ]);
     }


     return redirect()->action('PatientTestController@testdata', ['id' =>  $appointment]);

           }
           public function destroytest($id)
           {
             $pttd=DB::table('patient_test_details')
             ->where('id',$id)
             ->first();

             DB::table("patient_test_details")->where('id',$id)->update(array('deleted'=>1));

       return redirect()->action('PatientTestController@testdata', ['id' => $pttd->appointment_id]);

       }




public function otherimagingPost(Request $request)
{
$now = Carbon::now();

    $appointment=$request->appointment;
    $test=$request->test;
    $clinical=$request->clinical;
    $cat_id=$request->cat_id;
    $target=$request->target;



$pttids= Patienttest::where('appointment_id',$appointment)
  ->first();
if (is_null($pttids)) {
$PatientTest = Patienttest ::create([
  'appointment_id' => $appointment,
  'test_status' => 0,
  'created_at' =>$now,
  'updated_at' => $now,
]);
    $ptid = $PatientTest->id;
     } else {
     // Already test exist - just get the id
      $ptid =$pttids->id;
      DB::table('patient_test')->where('id', $ptid)
      ->update(['test_status' => 0,]);
     }
     DB::table('appointments')->where('id', $appointment)
     ->update(['p_status' => 11,]);
$Uid1 = Auth::user()->id;
$now = Carbon::now();
$imagingId = DB::table('radiology_test_details')->insertGetId([
                 'patient_test_id' => $ptid,
                 'appointment_id' => $appointment,
                 'clinicalinfo' => $clinical,
                 'test_cat_id' => $cat_id,
                 'target' => $target,
                 'test' => $test,
                 'done' => 0,
                 'user_id' =>$Uid1,
                 'confirm' => 'N',
                 'created_at' => $now,
              ]);
return redirect()->action('PatientTestController@testesImage',[$appointment]);

}

public function Otherremove(Request $request)
{
  $now = Carbon::now();
  $test=$request->test;
  $cat_id=$request->cat_id;
  $appointment =$request->appointment;
  $id1 = Auth::user()->id;
  DB::table("radiology_test_details")->where('id',$test)
  ->update(['deleted' => 1,'deleted_by' =>$id1,'updated_at' =>$now,]);
  return redirect()->action('PatientTestController@testesImage',[$appointment]);

}


public function mriPost(Request $request)
{
$now = Carbon::now();

$appointment=$request->appointment;
$test=$request->test;
$clinical=$request->clinical;
$cat_id=$request->cat_id;
$target=$request->target;



$pttids= Patienttest::where('appointment_id',$appointment)
->first();
if (is_null($pttids)) {
$PatientTest = Patienttest ::create([
'appointment_id' => $appointment,
'test_status' => 0,
'created_at' =>$now,
'updated_at' => $now,
]);
$ptid = $PatientTest->id;
} else {
// Already test exist - just get the id
$ptid =$pttids->id;
DB::table('patient_test')->where('id', $ptid)
->update(['test_status' => 0,]);
}
DB::table('appointments')->where('id', $appointment)
->update(['p_status' => 11,]);
$Uid1 = Auth::user()->id;
$now = Carbon::now();
$imagingId = DB::table('radiology_test_details')->insertGetId([
          'patient_test_id' => $ptid,
          'appointment_id' => $appointment,
          'clinicalinfo' => $clinical,
          'test_cat_id' => $cat_id,
          'target' => $target,
          'test' => $test,
          'done' => 0,
          'user_id' =>$Uid1,
          'confirm' => 'N',
          'created_at' => $now,
       ]);
return redirect()->action('PatientTestController@testdatamri',[$appointment]);

}

public function mriTestremove(Request $request)
{
$now = Carbon::now();
$test=$request->test;
$cat_id=$request->cat_id;
$appointment =$request->appointment;
$id1 = Auth::user()->id;
DB::table("radiology_test_details")->where('id',$test)
->update(['deleted' => 1,'deleted_by' =>$id1,'updated_at' =>$now,]);
return redirect()->action('PatientTestController@testdatamri',[$appointment]);

}




public function ctTest(Request $request)
{
$now = Carbon::now();

$appointment=$request->appointment;
$test=$request->test;
$clinical=$request->clinical;
$cat_id=$request->cat_id;
$target=$request->target;



$pttids= Patienttest::where('appointment_id',$appointment)
->first();
if (is_null($pttids)) {
$PatientTest = Patienttest ::create([
'appointment_id' => $appointment,
'test_status' => 0,
'created_at' =>$now,
'updated_at' => $now,
]);
$ptid = $PatientTest->id;
} else {
// Already test exist - just get the id
$ptid =$pttids->id;
DB::table('patient_test')->where('id', $ptid)
->update(['test_status' => 0,]);
}
DB::table('appointments')->where('id', $appointment)
->update(['p_status' => 11,]);
$Uid1 = Auth::user()->id;
$now = Carbon::now();
$imagingId = DB::table('radiology_test_details')->insertGetId([
          'patient_test_id' => $ptid,
          'appointment_id' => $appointment,
          'clinicalinfo' => $clinical,
          'test_cat_id' => $cat_id,
          'target' => $target,
          'test' => $test,
          'done' => 0,
          'user_id' =>$Uid1,
          'confirm' => 'N',
          'created_at' => $now,
       ]);
return redirect()->action('PatientTestController@testsImaging',[$appointment]);

}

public function ctTestremove(Request $request)
{
$now = Carbon::now();
$test=$request->test;
$cat_id=$request->cat_id;
$appointment =$request->appointment;
$id1 = Auth::user()->id;
DB::table("radiology_test_details")->where('id',$test)
->update(['deleted' => 1,'deleted_by' =>$id1,'updated_at' =>$now,]);

return redirect()->action('PatientTestController@testsImaging',[$appointment]);

}


public function ultraTest(Request $request)
{
$now = Carbon::now();

$appointment=$request->appointment;
$test=$request->test;
$clinical=$request->clinical;
$cat_id=$request->cat_id;
$target=$request->target;



$pttids= Patienttest::where('appointment_id',$appointment)
->first();
if (is_null($pttids)) {
$PatientTest = Patienttest ::create([
'appointment_id' => $appointment,
'test_status' => 0,
'created_at' =>$now,
'updated_at' => $now,
]);
$ptid = $PatientTest->id;
} else {
// Already test exist - just get the id
$ptid =$pttids->id;
DB::table('patient_test')->where('id', $ptid)
->update(['test_status' => 0,]);
}
DB::table('appointments')->where('id', $appointment)
->update(['p_status' => 11,]);
$Uid1 = Auth::user()->id;
$now = Carbon::now();
$imagingId = DB::table('radiology_test_details')->insertGetId([
          'patient_test_id' => $ptid,
          'appointment_id' => $appointment,
          'clinicalinfo' => $clinical,
          'test_cat_id' => $cat_id,
          'target' => $target,
          'test' => $test,
          'done' => 0,
          'user_id' =>$Uid1,
          'confirm' => 'N',
          'created_at' => $now,
       ]);
return redirect()->action('PatientTestController@testdataultra',[$appointment]);

}

public function ultraTestremove(Request $request)
{
$now = Carbon::now();
$test=$request->test;
$cat_id=$request->cat_id;
$appointment =$request->appointment;
$id1 = Auth::user()->id;
DB::table("radiology_test_details")->where('id',$test)
->update(['deleted' => 1,'deleted_by' =>$id1,'updated_at' =>$now,]);

return redirect()->action('PatientTestController@testdataultra',[$appointment]);

}


public function xrayTest(Request $request)
{
$now = Carbon::now();

$appointment=$request->appointment;
$test=$request->test;
$clinical=$request->clinical;
$cat_id=$request->cat_id;
$target=$request->target;



$pttids= Patienttest::where('appointment_id',$appointment)
->first();
if (is_null($pttids)) {
$PatientTest = Patienttest ::create([
'appointment_id' => $appointment,
'test_status' => 0,
'created_at' =>$now,
'updated_at' => $now,
]);
$ptid = $PatientTest->id;
} else {
// Already test exist - just get the id
$ptid =$pttids->id;
DB::table('patient_test')->where('id', $ptid)
->update(['test_status' => 0,]);
}
DB::table('appointments')->where('id', $appointment)
->update(['p_status' => 11,]);
$Uid1 = Auth::user()->id;
$now = Carbon::now();
$imagingId = DB::table('radiology_test_details')->insertGetId([
          'patient_test_id' => $ptid,
          'appointment_id' => $appointment,
          'clinicalinfo' => $clinical,
          'test_cat_id' => $cat_id,
          'target' => $target,
          'test' => $test,
          'done' => 0,
          'user_id' =>$Uid1,
          'confirm' => 'N',
          'created_at' => $now,
       ]);
return redirect()->action('PatientTestController@testdataxray',[$appointment]);

}

public function xrayTestremove(Request $request)
{
$now = Carbon::now();
$test=$request->test;
$cat_id=$request->cat_id;
$appointment =$request->appointment;
$id1 = Auth::user()->id;
DB::table("radiology_test_details")->where('id',$test)
->update(['deleted' => 1,'deleted_by' =>$id1,'updated_at' =>$now,]);

return redirect()->action('PatientTestController@testdataxray',[$appointment]);

}

public function imagingdestroytest($id)
{
$pttd=DB::table('radiology_test_details')
->where('id',$id)
->first();
DB::table("radiology_test_details")->where('id',$id)
->update(['deleted' =>1]);

return redirect()->action('PatientTestController@test_all', ['id' => $pttd->appointment_id]);

}
public function summPatients(Request $request)
{

  $appointment=$request->appointment_id;
  $med =$request->current_med;
  $doc_note =$request->complaints;
  $fam_note =$request->fam_note;
  $afya_user_id =$request->afya_user_id;

  $now = Carbon::now();
       if($doc_note){
         $psummary = DB::table('patient_summary')->where('appointment_id',$appointment)->first();
       if($psummary){
          DB::table('patient_summary')->where('appointment_id',$appointment)->update([
            'notes' => $doc_note,
            'updated_at' => $now,
         ]);
       }else{
        $patienttd = DB::table('patient_summary')->insert([
                 'appointment_id' => $appointment,
                 'notes' => $doc_note,
                 'created_at' => $now,
                 'updated_at' => $now,
              ]);
             }
           }
   if($fam_note){
               $pfam = DB::table('family_summary')->where('appointment_id',$appointment)->first();
             if($pfam){
                DB::table('family_summary')->where('appointment_id',$appointment)->update([
                  'notes' => $fam_note,
                  'updated_at' => $now,
               ]);
             }else{
              $patienttd = DB::table('family_summary')->insert([
                       'appointment_id' => $appointment,
                       'notes' => $fam_note,
                       'afya_user_id' => $afya_user_id,
                       'created_at' => $now,
                       'updated_at' => $now,
                    ]);
                   }
                 }
   if($med){
     $pmed = DB::table('current_medication')->where('appointment_id',$appointment)->first();
   if($pmed){
      DB::table('current_medication')->where('appointment_id',$appointment)->update([
        'drugs' => $med,
        'updated_at' => $now,
     ]);
   }else{
    $medds = DB::table('current_medication')->insert([
             'appointment_id' => $appointment,
             'drugs' => $med,
             'created_at' => $now,
             'updated_at' => $now,
          ]);
         }
}

$name1 = $request->name1;
$desc1 = $request->desc1;

if($name1){
  DB::table('self_reported_medical_history')->insert([
             'appointment_id' => $appointment,
             'afya_user_id' => $afya_user_id,
             'name' => $name1,
             'status' => $desc1,
             'created_at' => $now,
             'updated_at' => $now,
          ]);
    }
    $name2 = $request->name2;
    $desc2 = $request->desc2;
    if($name2){
      DB::table('self_reported_medical_history')->insert([
                 'appointment_id' => $appointment,
                 'afya_user_id' => $afya_user_id,
                 'name' => $name2,
                 'status' => $desc2,
                 'created_at' => $now,
                 'updated_at' => $now,
              ]);
        }
  $name3 = $request->name3;
  $desc3 = $request->desc3;
  if($name3){
    DB::table('self_reported_medical_history')->insert([
               'appointment_id' => $appointment,
               'afya_user_id' => $afya_user_id,
               'name' => $name3,
               'status' => $desc3,
               'created_at' => $now,
               'updated_at' => $now,
            ]);
      }
      $name4 = $request->name4;
      $desc4 = $request->desc4;
      if($name4){
        DB::table('self_reported_medical_history')->insert([
                   'appointment_id' => $appointment,
                   'afya_user_id' => $afya_user_id,
                   'name' => $name4,
                   'status' => $desc4,
                   'created_at' => $now,
                   'updated_at' => $now,
                ]);
          }
      $name5 = $request->name5;
      $desc5 = $request->desc5;
      if($name5){
        DB::table('self_reported_medical_history')->insert([
                   'appointment_id' => $appointment,
                   'afya_user_id' => $afya_user_id,
                   'name' => $name5,
                   'status' => $desc5,
                   'created_at' => $now,
                   'updated_at' => $now,
                ]);
          }


          $allergies1 = $request->allergies1;
          $status1 = $request->status1;
          if($allergies1){
            DB::table('afya_users_allergy')->insert([
                       'afya_user_id' => $afya_user_id,
                       'allergies' => $allergies1,
                       'status' => $status1,
                       'created_at' => $now,
                       'updated_at' => $now,
                    ]);
              }
              $allergies2 = $request->allergies2;
              $status2 = $request->status2;
              if($allergies2){
                DB::table('afya_users_allergy')->insert([
                           'afya_user_id' => $afya_user_id,
                           'allergies' => $allergies2,
                           'status' => $status2,
                           'created_at' => $now,
                           'updated_at' => $now,
                        ]);
                  }
                  $allergies3 = $request->allergies3;
                  $status3 = $request->status3;
                  if($allergies3){
                    DB::table('afya_users_allergy')->insert([
                               'afya_user_id' => $afya_user_id,
                               'allergies' => $allergies3,
                               'status' => $status3,
                               'created_at' => $now,
                               'updated_at' => $now,
                            ]);
                    }
                    $allergies4 = $request->allergies4;
                    $status4 = $request->status4;
                    if($allergies4){
                      DB::table('afya_users_allergy')->insert([
                                 'afya_user_id' => $afya_user_id,
                                 'allergies' => $allergies4,
                                 'status' => $status4,
                                 'created_at' => $now,
                                 'updated_at' => $now,
                              ]);
                      }

          $allergies5 = $request->allergies5;
          $status5 = $request->status5;
          if($allergies5){
            DB::table('afya_users_allergy')->insert([
                       'afya_user_id' => $afya_user_id,
                       'allergies' => $allergies5,
                       'status' => $status5,
                       'created_at' => $now,
                       'updated_at' => $now,
                    ]);
            }

$bowel =$request->bowel;
$bowel_details =$request->bowel_details;
$urinary =$request->urinary;
$urinary_details =$request->urinary_details;
$sleep =$request->sleep;
$sleep_details =$request->sleep_details;
$appetite =$request->appetite;
$appetite_details =$request->appetite_details;

if($bowel){
$bowl = DB::table('patient_systemic')->insert([
         'appointment_id' => $appointment,
         'type' => $bowel,
         'description' => $bowel_details,
         'created_at' => $now,
         'updated_at' => $now,   ]);
}
if($urinary){
$urin= DB::table('patient_systemic')->insert([
         'appointment_id' => $appointment,
         'type' => $urinary,
         'description' => $urinary_details,
         'created_at' => $now,
         'updated_at' => $now,   ]);
}

if($sleep){
$slee= DB::table('patient_systemic')->insert([
         'appointment_id' => $appointment,
         'type' => $sleep,
         'description' => $sleep_details,
         'created_at' => $now,
         'updated_at' => $now,   ]);
}
if($appetite){
$appet = DB::table('patient_systemic')->insert([
         'appointment_id' => $appointment,
         'type' => $appetite,
         'description' => $appetite_details,
         'created_at' => $now,
         'updated_at' => $now,   ]);
}

return redirect()->action('PatientController@examination', ['id' =>  $appointment]);
}

public function trgpost(Request $request)

{
  $appointment = $request->appointment_id;
  $id = $request->id;
  $weight = $request->weight;
  $heightS = $request->current_height;
  $temperature = $request->temperature;
  $systolic = $request->systolic;
  $diastolic = $request->diastolic;
  $allergies = $request->allergies;
  $chiefcompliant = $request->chiefcomplaint;
  $observation = $request->observation;
  $symptoms = $request->symptoms;
  $nurse = $request->nurse;
  $doctor = $request->doctor;
  $lmp = $request->lmp;
  $rbs = $request->rbs;
  $hr = $request->hr;
  $rr = $request->rr;
  $pregnant = $request->pregnant;

        if($chiefcompliant){ $chiefcompliant = implode(',', $chiefcompliant); }
                if($symptoms){$symptoms= implode(',', $symptoms); }
                if($observation){$observation= implode(',', $observation); }

$ptriage = DB::table('triage_details')->where('appointment_id',$appointment)->first();
if($ptriage){
  DB::table('triage_details')->where('appointment_id',$appointment)->update([
    'current_weight'=> $weight,
    'current_height'=>$heightS,
    'temperature'=>$temperature,
    'systolic_bp'=>$systolic,
    'diastolic_bp'=>$diastolic,
    'chief_compliant'=>$chiefcompliant,
    'observation'=>$observation,
    'symptoms'=>$symptoms,
    'nurse_notes'=>'',
    'pregnant'=>$pregnant,
    'lmp'=>$lmp,
    'Doctor_note'=>$nurse,
    'prescription'=>'',
    'rbs'=>$rbs,
    'hr'=>$hr,
    'rr'=>$rr,
    'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
  ]);

}else{
                DB::table('triage_details')->insert([
                  'appointment_id' => $appointment,
                  'current_weight'=> $weight,
                  'current_height'=>$heightS,
                  'temperature'=>$temperature,
                  'systolic_bp'=>$systolic,
                  'diastolic_bp'=>$diastolic,
                  'chief_compliant'=>$chiefcompliant,
                  'observation'=>$observation,
                  'symptoms'=>$symptoms,
                  'nurse_notes'=>'',
                  'pregnant'=>$pregnant,
                  'lmp'=>$lmp,
                  'Doctor_note'=>$nurse,
                  'prescription'=>'',
                  'rbs'=>$rbs,
                  'hr'=>$hr,
                  'rr'=>$rr,
                  'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                  'updated_at' => \Carbon\Carbon::now()->toDateTimeString()]

                );
              }
                DB::table('appointments')->where('id',$appointment)->update([
                  'status'=>2,
                ]);


                return redirect()->action('PatientController@patsamury', ['id' =>  $appointment]);
  }

public function ImpressionSave(Request $request)
{
  $appointment=$request->appointment_id;
  $doc_note =$request->doc_note;
  $now = Carbon::now();
if($doc_note){
$ge = DB::table('impression')->where('appointment_id',$appointment)->first();
if($ge){
$patienttd = DB::table('impression')->where('appointment_id',$appointment)->update([
'notes' => $doc_note,
'updated_at' => $now,
]);
}else{
$patienttd = DB::table('impression')->insert([
'appointment_id' => $appointment,
'notes' => $doc_note,
'created_at' => $now,
'updated_at' => $now,
]);
}

}

return redirect()->action('PatientTestController@alltestdata', ['id' =>  $appointment]);
}


public function mrPatients(Request $request)
{

  $appointment=$request->appointment_id;
  $afya_user_id =$request->afya_user_id;
  $med =$request->current_med;
  $doc_note =$request->doc_note;
  $diagnosis =$request->diagnosis;
  $impression =$request->impression;
  $now = Carbon::now();
  $Uid1 = Auth::user()->id;
       if($doc_note){
        $patienttd = DB::table('patient_summary')->insert([
                 'appointment_id' => $appointment,
                 'notes' => $doc_note,
                 'user_id' => $Uid1,
                 'created_at' => $now,
                 'updated_at' => $now,
              ]);
             }
   if($med){
    $medds = DB::table('current_medication')->insert([
             'appointment_id' => $appointment,
             'drugs' => $med,
             'user_id' => $Uid1,
             'created_at' => $now,
             'updated_at' => $now,
          ]);
         }
         if($diagnosis){
          $medds = DB::table('patient_diagnosis')->insert([
                   'appointment_id' => $appointment,
                    'afya_user_id' => $afya_user_id,
                   'disease_id' => $diagnosis,
                   'date_diagnosed' => $now,
                   'created_at' => $now,
                   'updated_at' => $now,
                ]);
               }
// impression
if($impression){
 $imp = DB::table('impression')->insert([
          'appointment_id' => $appointment,
           'notes' => $impression,
          'created_at' => $now,
          'updated_at' => $now,
       ]);
      }
return redirect()->action('DoctorController@edithistory', ['id' =>  $appointment]);
}

public function mrPatients2(Request $request)
{

  $appointment=$request->appointment_id;
  $afya_user_id =$request->afya_user_id;
  $med =$request->current_med;
  $doc_note =$request->doc_note;
  $diagnosis =$request->diagnosis;
  $impression =$request->impression;
  $now = Carbon::now();
  $Uid1 = Auth::user()->id;
       if($doc_note){
        $patienttd = DB::table('patient_summary')->insert([
                 'appointment_id' => $appointment,
                 'notes' => $doc_note,
                 'user_id' => $Uid1,
                 'created_at' => $now,
                 'updated_at' => $now,
              ]);
             }
   if($med){
    $medds = DB::table('current_medication')->insert([
             'appointment_id' => $appointment,
             'drugs' => $med,
             'user_id' => $Uid1,
             'created_at' => $now,
             'updated_at' => $now,
          ]);
         }
         if($diagnosis){
          $medds = DB::table('patient_diagnosis')->insert([
                   'appointment_id' => $appointment,
                    'afya_user_id' => $afya_user_id,
                   'disease_id' => $diagnosis,
                   'date_diagnosed' => $now,
                   'created_at' => $now,
                   'updated_at' => $now,
                ]);
               }
// impression
if($impression){
 $imp = DB::table('impression')->insert([
          'appointment_id' => $appointment,
           'notes' => $impression,
          'created_at' => $now,
          'updated_at' => $now,
       ]);
      }
return redirect()->action('DoctorController@edithistory2', ['id' =>  $appointment]);
}
}
