<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Patienttest;
use Illuminate\Support\Facades\Input;
use Auth;
use Carbon\Carbon;

class PatientTestController2 extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function xrayreports(Request $request)
     {

       $appointment=$request->get('appointment_id');
       $rtd_id = $request->get('rtd_id');

        $tsts1 = DB::table('radiology_test_details')
        ->Join('xray', 'radiology_test_details.test', '=', 'xray.id')
      ->Join('appointments', 'radiology_test_details.appointment_id', '=', 'appointments.id')
      ->Join('doctors', 'appointments.doc_id', '=', 'doctors.id')
      ->Join('facilities', 'appointments.facility_id', '=', 'facilities.FacilityCode')
      ->select('radiology_test_details.*','xray.name','facilities.FacilityName','doctors.name as docname')
        ->where('radiology_test_details.id', '=',$rtd_id)
        ->first();

        $patientD=DB::table('appointments')
      ->leftjoin('afya_users','appointments.afya_user_id','=','afya_users.id')
      ->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
        ->leftjoin('facilities','appointments.facility_id','=','facilities.FacilityCode')
        ->select('appointments.*','afya_users.firstname','afya_users.dob','afya_users.secondName','afya_users.gender',
        'facilities.set_up','patient_admitted.condition')
        ->where('appointments.id',$appointment)
        ->first();
    return view('doctor.tests.reportxray')->with('tsts1',$tsts1)->with('patientD',$patientD);
     }

     public function ctreports(Request $request)
     {
     $appointment=$request->get('appointment_id');
       $rtd_id = $request->get('rtd_id');

        $tsts1 = DB::table('radiology_test_details')
        ->Join('ct_scan', 'radiology_test_details.test', '=', 'ct_scan.id')
      ->Join('appointments', 'radiology_test_details.appointment_id', '=', 'appointments.id')
      ->Join('doctors', 'appointments.doc_id', '=', 'doctors.id')
      ->Join('facilities', 'appointments.facility_id', '=', 'facilities.FacilityCode')
      ->select('radiology_test_details.*','ct_scan.name','facilities.FacilityName','doctors.name as docname')
        ->where('radiology_test_details.id', '=',$rtd_id)
        ->first();

        $patientD=DB::table('appointments')
      ->leftjoin('afya_users','appointments.afya_user_id','=','afya_users.id')
      ->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
        ->leftjoin('facilities','appointments.facility_id','=','facilities.FacilityCode')
        ->select('appointments.*','afya_users.firstname','afya_users.dob','afya_users.secondName','afya_users.gender',
        'facilities.set_up','patient_admitted.condition')
        ->where('appointments.id',$appointment)
        ->first();
    return view('doctor.tests.reportct')->with('tsts1',$tsts1)->with('patientD',$patientD);
     }

public function otherReport(Request $request)
{
 $appointment=$request->get('appointment_id');
 $rtd_id = $request->get('rtd_id');

  $tsts1 = DB::table('radiology_test_details')
  ->Join('other_tests', 'radiology_test_details.test', '=', 'other_tests.id')
->Join('appointments', 'radiology_test_details.appointment_id', '=', 'appointments.id')
->Join('doctors', 'appointments.doc_id', '=', 'doctors.id')
->Join('facilities', 'appointments.facility_id', '=', 'facilities.FacilityCode')
->select('radiology_test_details.*','other_tests.name','facilities.FacilityName','doctors.name as docname')
  ->where('radiology_test_details.id', '=',$rtd_id)
  ->first();

  $patientD=DB::table('appointments')
->leftjoin('afya_users','appointments.afya_user_id','=','afya_users.id')
->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
  ->leftjoin('facilities','appointments.facility_id','=','facilities.FacilityCode')
  ->select('appointments.*','afya_users.firstname','afya_users.dob','afya_users.secondName','afya_users.gender',
  'facilities.set_up','patient_admitted.condition')
  ->where('appointments.id',$appointment)
  ->first();
return view('doctor.tests.otherReport')->with('tsts1',$tsts1)->with('patientD',$patientD);
}




     public function mrireports(Request $request)
     {
      $appointment=$request->get('appointment_id');
      $rtd_id = $request->get('rtd_id');


        $tsts1 = DB::table('radiology_test_details')
        ->Join('mri_tests', 'radiology_test_details.test', '=', 'mri_tests.id')
      ->Join('appointments', 'radiology_test_details.appointment_id', '=', 'appointments.id')
      ->Join('doctors', 'appointments.doc_id', '=', 'doctors.id')
      ->Join('facilities', 'appointments.facility_id', '=', 'facilities.FacilityCode')
      ->select('radiology_test_details.*','mri_tests.name','facilities.FacilityName','doctors.name as docname')
        ->where('radiology_test_details.id', '=',$rtd_id)
        ->first();

        $patientD=DB::table('appointments')
      ->leftjoin('afya_users','appointments.afya_user_id','=','afya_users.id')
      ->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
        ->leftjoin('facilities','appointments.facility_id','=','facilities.FacilityCode')
        ->select('appointments.*','afya_users.firstname','afya_users.dob','afya_users.secondName','afya_users.gender',
        'facilities.set_up','patient_admitted.condition')
        ->where('appointments.id',$appointment)
        ->first();
    return view('doctor.tests.reportmri')->with('tsts1',$tsts1)->with('patientD',$patientD);
     }

     public function ultrareports(Request $request)
     {


     $appointment=$request->get('appointment_id');
     $rtd_id = $request->get('rtd_id');

      $tsts1 = DB::table('radiology_test_details')
      ->Join('ultrasound', 'radiology_test_details.test', '=', 'ultrasound.id')
    ->Join('appointments', 'radiology_test_details.appointment_id', '=', 'appointments.id')
    ->Join('doctors', 'appointments.doc_id', '=', 'doctors.id')
    ->Join('facilities', 'appointments.facility_id', '=', 'facilities.FacilityCode')
    ->select('radiology_test_details.*','ultrasound.name','facilities.FacilityName','doctors.name as docname')
      ->where('radiology_test_details.id', '=',$rtd_id)
      ->first();

      $patientD=DB::table('appointments')
    ->leftjoin('afya_users','appointments.afya_user_id','=','afya_users.id')
    ->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
      ->leftjoin('facilities','appointments.facility_id','=','facilities.FacilityCode')
      ->select('appointments.*','afya_users.firstname','afya_users.dob','afya_users.secondName','afya_users.gender',
      'facilities.set_up','patient_admitted.condition')
      ->where('appointments.id',$appointment)
      ->first();
    return view('doctor.tests.reportultra')->with('tsts1',$tsts1)->with('patientD',$patientD);
     }

     public function testdetails($id)
     {

    $pdetails = DB::table('patient_test')
    ->leftJoin('appointments', 'patient_test.appointment_id', '=', 'appointments.id')
    ->Join('facilities', 'appointments.facility_id', '=', 'facilities.FacilityCode')
    ->Join('doctors', 'appointments.doc_id', '=', 'doctors.id')
    ->leftJoin('patient_admitted', 'appointments.id', '=', 'patient_admitted.appointment_id')
    ->select('appointments.persontreated','appointments.afya_user_id','appointments.id as appId',
    'doctors.name as docname','facilities.FacilityName','patient_admitted.condition')
    ->where('patient_test.id', '=',$id)
    ->first();

  $tsts = DB::table('patient_test')
  ->leftJoin('appointments', 'patient_test.appointment_id', '=', 'appointments.id')
  ->leftJoin('patient_test_details', 'patient_test.id', '=', 'patient_test_details.patient_test_id')
  ->leftJoin('icd10_option', 'patient_test_details.conditional_diag_id', '=', 'icd10_option.id')
  ->leftJoin('tests', 'patient_test_details.tests_reccommended', '=', 'tests.id')
  ->leftJoin('test_subcategories', 'tests.sub_categories_id', '=', 'test_subcategories.id')
  ->leftJoin('test_categories', 'test_subcategories.categories_id', '=', 'test_categories.id')
  ->select('tests.name as tname','test_subcategories.name as tsname','icd10_option.name as dname','patient_test_details.created_at as date',
  'patient_test_details.id as patTdid','test_categories.name as tcname','patient_test_details.done',
  'patient_test_details.tests_reccommended','appointments.id as AppId')
  ->where('patient_test.id', '=',$id)
  ->get();

           $rady = DB::table('patient_test')
               ->leftJoin('appointments', 'patient_test.appointment_id', '=', 'appointments.id')
               ->Join('radiology_test_details', 'patient_test.appointment_id', '=', 'radiology_test_details.appointment_id')
               ->leftJoin('test_categories', 'radiology_test_details.test_cat_id', '=', 'test_categories.id')
               ->select('radiology_test_details.created_at as date','radiology_test_details.test',
               'radiology_test_details.clinicalinfo','radiology_test_details.test_cat_id','radiology_test_details.done',
               'radiology_test_details.id as patTdid','test_categories.name as tcname')
                ->where('patient_test.id', '=',$id)
                ->get();
       return view('doctor.tests.tstdetails')->with('tsts',$tsts)->with('pdetails',$pdetails)->with('rady',$rady);
     }

     public function otherResult(Request $request)
     {
     $appointment=$request->appointment_id;
     $rtdid =$request->rtdid;
     $notes =$request->note;
     $technique =$request->technique;
     $u_id = Auth::user()->id;

     if ($notes)  {
       DB::table('radiology_test_result')->insert(
         [
           'radiology_td_id' => $rtdid,
           'results' => $notes,
           'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
           'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
         ]);

     DB::table('radiology_test_details')
           ->where('id',$rtdid)
           ->update([
          'technique' => $technique,
           'done' => 1,
           'conclusion'=>$notes,
           'status'=> 1,
           'result_by'=>$u_id,
           'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
         ]);
     }
     return redirect()->action('PatientTestController@test_all',[$appointment]);
     }

     public function mriResult(Request $request)
     {
     $appointment=$request->appointment_id;
     $rtdid =$request->rtdid;
     $notes =$request->note;
     $technique =$request->technique;
     $u_id = Auth::user()->id;
     if ($notes)  {

       DB::table('radiology_test_result')->insert(
         [
           'radiology_td_id' => $rtdid,
           'results' => $notes,
           'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
           'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
         ]);


     DB::table('radiology_test_details')
           ->where('id',$rtdid)
           ->update([
             'technique' => $technique,
           'done' => 1,
           'conclusion'=>$notes,
           'status'=> 1,
           'result_by'=>$u_id,
           'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
         ]);
     }
     return redirect()->action('PatientTestController@test_all',[$appointment]);
     }

     public function ultraResult(Request $request)
     {
     $appointment=$request->appointment_id;
     $rtdid =$request->rtdid;
     $notes =$request->note;
     $technique =$request->technique;
     $u_id = Auth::user()->id;
     if ($notes)  {

       DB::table('radiology_test_result')->insert(
         [
           'radiology_td_id' => $rtdid,
           'results' => $notes,
           'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
           'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
         ]);



     DB::table('radiology_test_details')
           ->where('id',$rtdid)
           ->update([
             'technique' => $technique,
           'done' => 1,
           'conclusion'=>$notes,
           'status'=> 1,
           'result_by'=>$u_id,
           'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
         ]);
     }
     return redirect()->action('PatientTestController@test_all',[$appointment]);
     }

     public function ctResult(Request $request)
     {
     $appointment=$request->appointment_id;
     $rtdid =$request->rtdid;
     $notes =$request->note;
     $technique =$request->technique;
     $u_id = Auth::user()->id;
     if ($notes)  {
       DB::table('radiology_test_result')->insert(
         [
           'radiology_td_id' => $rtdid,
           'results' => $notes,
           'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
           'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
         ]);




     DB::table('radiology_test_details')
           ->where('id',$rtdid)
           ->update([
             'technique' => $technique,
           'done' => 1,
           'conclusion'=>$notes,
           'status'=> 1,
           'result_by'=>$u_id,
           'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
         ]);
     }
     return redirect()->action('PatientTestController@test_all',[$appointment]);
     }

     public function xrayResult(Request $request)
     {
     $appointment=$request->appointment_id;
     $rtdid =$request->rtdid;
     $notes =$request->note;
     $technique =$request->technique;
     $u_id = Auth::user()->id;
     if ($notes)  {

       DB::table('radiology_test_result')->insert(
         [
           'radiology_td_id' => $rtdid,
           'results' => $notes,
           'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
           'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
         ]);


     DB::table('radiology_test_details')
           ->where('id',$rtdid)
           ->update([
             'technique' => $technique,
           'done' => 1,
           'conclusion'=>$notes,
           'status'=> 1,
           'result_by'=>$u_id,
           'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
         ]);
     }


     return redirect()->action('PatientTestController@test_all',[$appointment]);

     }
     public function labResult(Request $request)
     {
     $appointment=$request->appointment;
     $ptdid =$request->ptdid;
     $ptid =$request->ptid;
     $notes =$request->note;
     $test =$request->test;

     $u_id = Auth::user()->id;

     if ($notes)  {
       DB::table('test_results')->insert(
         [
           'patient_test' => $ptid,
           'ptd_id' => $ptdid,
           'tests_id'=> $test,
           'value'=>$notes,
           'status'=>1,
           'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
           'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
         ]);
     DB::table('patient_test_details')
           ->where('id',$ptdid)
           ->update([
           'done' => 1,
           'status'=> 1,
           'user_id'=>$u_id,
           'updated_at' => \Carbon\Carbon::now()->toDateTimeString()
         ]);
     }
     return redirect()->action('PatientTestController@test_all',[$appointment]);

     }

     public function imagingPost(Request $request)
     {
$now = Carbon::now();
         $appointment=$request->appointment;
         $test=$request->test;
         $clinical=$request->clinical;
         $cat_id=$request->cat_id;
         $target=$request->target;

$pttids= Patienttest::where('appointment_id',$appointment)
       ->first();
if (is_null($pttids)) {
     $PatientTest = Patienttest ::create([
       'appointment_id' => $appointment,
       'test_status' => 0,
       'created_at' =>$now,
       'updated_at' => $now,
     ]);
         $ptid = $PatientTest->id;
          } else {
          // Already test exist - just get the id
           $ptid =$pttids->id;
           DB::table('patient_test')->where('id', $ptid)
           ->update(['test_status' => 0,]);
          }
          DB::table('appointments')->where('id', $appointment)
          ->update(['p_status' => 11,]);
$Uid1 = Auth::user()->id;
$now = Carbon::now();
$imagingId = DB::table('radiology_test_details')->insertGetId([
                      'patient_test_id' => $ptid,
                      'appointment_id' => $appointment,
                      'clinicalinfo' => $clinical,
                      'test_cat_id' => $cat_id,
                      'target' => $target,
                      'test' => $test,
                      'done' => 0,
                      'user_id' =>$Uid1,
                      'confirm' => 'N',
                      'created_at' => $now,
                   ]);
   return redirect()->action('PatientTestController@alltestdata',[$appointment]);
 }
 public function Radremove(Request $request)
 {
   $now = Carbon::now();
   $test=$request->test;
   $cat_id=$request->cat_id;
   $appointment =$request->appointment;
   $id1 = Auth::user()->id;
   DB::table("radiology_test_details")->where('id',$test)
   ->update(['deleted' => 1,'deleted_by' =>$id1,'updated_at' =>$now,]);

   return redirect()->action('PatientTestController@alltestdata',[$appointment]);

}
public function storeRegLab(Request $request)
{

     $appointment=$request->appointment_id;
     $test_id=$request->test_id;
     $docnote=$request->docnote;
$Uid1 = Auth::user()->id;
 $pttids= Patienttest::where('appointment_id',$appointment)
  ->first();

     if (is_null($pttids)) {
$PatientTest = Patienttest ::create([
  'appointment_id' => $appointment,
  'test_status' => 0,
]);
    $ptid = $PatientTest->id;
     } else {
     // Already test exist - just get the id
      $ptid =$pttids->id;
      DB::table('patient_test')->where('id', $ptid)
      ->update(['test_status' => 0,]);
     }
     DB::table('appointments')->where('id', $appointment)
     ->update(['p_status' => 11,]);

     // Inserting  tests
$patient_td = DB::table('patient_test_details')->insertGetId([
                  'patient_test_id' => $ptid,
                  'appointment_id' => $appointment,
                  'tests_reccommended' => $test_id,
                  'done' => 0,
                   'user_id' =>$Uid1,
               ]);
   // Inserting patientNotes tests
if ($docnote) {
$patientNotes = DB::table('patientNotes')->insert([
    'appointment_id' => $appointment,
    'note' => $docnote,
    'target' => 'Test',
    'ptd_id' => $patient_td,
     ]);
}
// $afyaId = DB::table("appointments")->where('id',$appointment)->select('afya_user_id')->first();
return redirect()->action('PatientTestController@alltestdata',[$appointment]);
  }

  public function labremove(Request $request)
  {
    $now = Carbon::now();
    $test=$request->ptd_id;
    $appointment =$request->appointment_id;
    $id1 = Auth::user()->id;

    DB::table("patient_test_details")->where('id',$test)
    ->update(['deleted' => 1,'deleted_by' =>$id1,'updated_at' =>$now,]);

    return redirect()->action('PatientTestController@alltestdata',[$appointment]);

  }
}
