<nav class="navbar-default navbar-static-side" role="navigation">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element">
                   <!-- <span><img alt="user" class="img-circle" src="img/profile_small.jpg" /></span> -->
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>
                    </span> <span class="text-muted text-xs block">{{ Auth::user()->role }} <b class="caret"></b></span> </span> </a>
                    <ul class="dropdown-menu animated fadeInRight m-t-xs">
                        <li><a href="#">Profile</a></li>
                        <li><a href="#">Contacts</a></li>
                        <li><a href="#">Mailbox</a></li>
                        <li class="divider"></li>
                        <li><a href="{{ url('/logout') }}">Logout</a></li>

                    </ul>
                </div>
                <div class="logo-element">
                    Afya+
                </div>
            </li>
            <?php
              use Carbon\Carbon;
              $today = Carbon::today();



$doc_id = DB::table('facility_doctor')
        ->Join('facilities', 'facility_doctor.facilitycode', '=', 'facilities.FacilityCode')
        ->select('facility_doctor.doctor_id','facilities.set_up','facilities.FacilityCode')
        ->where('facility_doctor.user_id', Auth::user()->id)
        ->first();

$doctor_id=$doc_id->doctor_id;
$setUp=$doc_id->set_up;
$f_code = $doc_id->FacilityCode;
$allpatients=DB::table('appointments')->distinct('afya_user_id')
->where('facility_id', $f_code)->count('afya_user_id');

if($setUp =='Partial'){
$newpatientpartial1 =  DB::table('appointments')
->where([ ['appointments.created_at','>=',$today],
['appointments.status', '=', 1],
['appointments.facility_id', '=', $f_code],
['appointments.doc_id', '=',$doctor_id],
])->count();

$newpatientpartial2 =  DB::table('appointments')
->where([ ['appointments.created_at','>=',$today],
['appointments.facility_id', '=', $f_code],
['appointments.status', '=', 2],
['appointments.doc_id', '=',$doctor_id],
])
->count();
$newpatientpartial =($newpatientpartial1 + $newpatientpartial2);
}else{
$newpatientpartial =  DB::table('appointments')
->where([ ['appointments.created_at','>=',$today],
['appointments.status', '=', 2],
['appointments.doc_id', '=',$doctor_id],
])->count();



}

?>

<li><a href="{{ url('private') }}"><i class="fa fa-th-large"></i><span> Waiting List </span>   <span class="badge"><?php echo $newpatientpartial; ?></span></a></li>
<!-- <li class="{{ isActiveRoute('endvisit') }}"><a  href="{{route('endvisit',$app_id)}}"><i class="fa fa-close"></i>End Visit</a></li> -->

<li class="{{ isActiveRoute('showPatient') }}"><a  href="{{route('showPatient',$app_id)}}"><i class="fa fa-briefcase"></i> Patient Details</a></li>
<li class="{{ isActiveRoute('nurseVitals') }}"><a  href="{{route('nurseVitals',$app_id)}}"><i class="fa fa-thermometer"></i>Triage</a></li>
    <li class="{{ isActiveRoute('patsamury') }}"><a  href="{{route('patsamury',$app_id)}}"><i class="fa fa-pencil"></i>Patient Summary</a></li>
   <li class="{{ isActiveRoute('examination') }}"><a  href="{{route('examination',$app_id)}}"><i class="fa fa-pencil-square"></i>Examination</a></li>
    <li class="{{ isActiveRoute('impression') }}"><a  href="{{route('impression',$app_id)}}"><i class="fa fa-pencil-square-o"></i>Impression</a></li>
    <li class="{{ isActiveRoute('alltestes') }}"><a  href="{{route('alltestes',$app_id)}}"><i class="fa fa-stethoscope"></i>Tests</a></li>
    <li class="{{ isActiveRoute('medicines') }}"><a href="{{route('medicines',$app_id)}}"><i class="fa fa-plus-square"></i>Prescriptions</a></li>
    <li class="{{ isActiveRoute('procedure') }}"><a  href="{{route('procedure',$app_id)}}"><i class="fa fa-eyedropper"></i>Procedures</a></li>

  @if ($condition =='Admitted')
      <li class="{{ isActiveRoute('discharge') }}"><a  href="{{route('discharge',$app_id)}}"><i class="fa fa-stethoscope"></i>Discharge</a></li>
     @else
      <li class="{{ isActiveRoute('admit') }}"><a  href="{{route('admit',$app_id)}}"><i class="fa fa-hospital-o"></i>Admit</a></li>
      @endif
      <li class="{{ isActiveRoute('transfering') }}"><a  href="{{route('transfering',$app_id)}}"><i class="fa fa-external-link-square"></i>Referral</a></li>
      <li class="{{ isActiveRoute('patienthistory') }}"><a href="{{route('patienthistory',$app_id)}}"><i class="fa fa-file-text"></i>Medical Report</a></li>

      <li class="{{ isActiveRoute('appcalendar') }}"><a href="{{route('calendar.show',$afyauserId)}}"><i class="fa fa-calendar"></i>Next Visit</a><li>

  <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout<span class="fa arrow"></span></a></li>

           </ul>

    </div>
</nav>
