@extends('layouts.doctor_layout')
@section('title', 'Triage')
@section('content')
<?php
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){
$Did = $Docdata->id;
$Name = $Docdata->name;
$Address = $Docdata->address;
$RegNo = $Docdata->regno;
$RegDate = $Docdata->regdate;
$Speciality = $Docdata->speciality;
$Sub_Speciality = $Docdata->subspeciality;
$set_up = $Docdata->set_up;
}
?>

<?php


   $stat= $pdetails->status;
   $afyauserId= $pdetails->afya_user_id;
    $dependantId= $pdetails->persontreated;
    $app_id =  $pdetails->id;
    $doc_id= $pdetails->doc_id;
    $fac_id= $pdetails->facility_id;
    $fac_setup= $pdetails->set_up;
  $condition = $pdetails->condition;

?>



@section('leftmenu')
@include('includes.doc_inc.leftmenu2')
@endsection
<!--tabs Menus-->
@include('includes.doc_inc.topnavbar_v2')


<!--tabs Menus-->
<div class="row m-t-lg">
          <div class="col-lg-12">
              <div class="tabs-container">

                  <div class="tabs-left">
                      <ul class="nav nav-tabs">
                          <li class="active"><a data-toggle="tab" href="#tab-1">Presenting Complaints</a></li>
                          <li class=""><a data-toggle="tab" href="#tab-2">Current Medication</a></li>
                          <li class=""><a data-toggle="tab" href="#tab-3">Past Medical History</a></li>
                          <li class=""><a data-toggle="tab" href="#tab-4">Allergies</a></li>
                          <li class=""><a data-toggle="tab" href="#tab-5">Systemic Inquiry</a></li>
                          <li class=""><a data-toggle="tab" href="#tab-6">Family History</a></li>




                      </ul>
                      <div class="tab-content ">

    <div id="tab-1" class="tab-pane active">
        <div class="panel-body">
          <form class="form-horizontal" role="form" method="POST" action="/summPatients" >
          <input type="hidden" name="_token" value="{{ csrf_token() }}">
          {{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
          {{ Form::hidden('afya_user_id',$afyauserId, array('class' => 'form-control')) }}

          <div class="form-group">
            <label class="control-label">Presenting Complaints </label><br>
              <div class="col-lg-10">
              <textarea class="form-control" rows="5"  name="complaints">@foreach($psummary as $psumm){{$psumm->notes}}  @endforeach</textarea>
            </div>
          </div>

        </div>
    </div>
<div id="tab-2" class="tab-pane">
<div class="panel-body">
  <div class="form-group">
    <label class="control-label">Current Medication </label><br>
      <div class="col-lg-10">
      <textarea class="form-control" rows="3"  name="current_med">
        @foreach($cmed as $cmeds)
      {{$cmeds->drugs}}
      @endforeach
    </textarea>
      </div>
  </div>
</div>
</div>
<div id="tab-3" class="tab-pane">
  <div class="panel-body">

      <div class="table-responsive">
        <table class="table borderless" id="employee_table" align=center>


          <tr id="con1">
            <td><input type="text" name="name1" placeholder="Condition Name" class="form-control"></td>
            <td><input type="text" name="desc1" placeholder="Description" class="form-control"></td>
          </tr>

          <tr>
          <td>  <input type="button" id="cond2" value="ADD MORE" class='btn btn-primary'></td>
          </tr>
          <tr id="con2" class="ficha">
            <td><input type="text" name="name2" placeholder="Condition Name" class="form-control"></td>
            <td><input type="text" name="desc2" placeholder="Description" class="form-control"></td>
          </tr>

          <tr>
          <td>  <input type="button" id="cond3" value="ADD MORE" class='btn btn-primary ficha'></td>
          </tr>
          <tr id="con3" class="ficha">
            <td><input type="text" name="name3" placeholder="Condition Name" class="form-control"></td>
            <td><input type="text" name="desc3" placeholder="Description" class="form-control"></td>
          </tr>

          <tr>
          <td>  <input type="button" id="cond4" value="ADD MORE" class='btn btn-primary ficha'></td>
          </tr>
          <tr id="con4" class="ficha">
            <td><input type="text" name="name4" placeholder="Condition Name" class="form-control"></td>
            <td><input type="text" name="desc4" placeholder="Description" class="form-control"></td>
          </tr>

          <tr>
          <td>  <input type="button" id="cond5" value="ADD MORE" class='btn btn-primary ficha'></td>
          </tr>
          <tr id="con5" class="ficha">
            <td><input type="text" name="name5" placeholder="Condition Name" class="form-control"></td>
            <td><input type="text" name="desc5" placeholder="Description" class="form-control"></td>
          </tr>



        </table>
    </div>
</div>
</div>
<div id="tab-4" class="tab-pane">
<div class="panel-body">

  <div class="table-responsive">
    <table class="table borderless" id="procedure_table" align=center>
<tr id="row1">
        <td><input type="text" name="allergies1" placeholder="Allergy" class="form-control"></td>
        <td><input type="text" name="status1"  placeholder="Description" class="form-control"></td>
</tr>
<tr>
<td>  <input type="button" id="aller1" value="ADD MORE" class='btn btn-primary'></td>
</tr>
      <tr id="allerge2" class="ficha">
        <td><input type="text" name="allergies2" placeholder="Allergy" class="form-control"></td>
        <td><input type="text" name="status2"  placeholder="Description" class="form-control"></td>

      </tr>
      <tr>
      <td>  <input type="button" id="aller2" value="ADD MORE" class='btn btn-primary ficha'></td>
      </tr>
            <tr id="allerge3" class="ficha">
        <td><input type="text" name="allergies3" placeholder="Allergy" class="form-control"></td>
        <td><input type="text" name="status3"  placeholder="Description" class="form-control"></td>
      </tr>
      <tr>
      <td>  <input type="button" id="aller3" value="ADD MORE" class='btn btn-primary ficha'></td>
      </tr>
            <tr id="allerge4" class="ficha">
        <td><input type="text" name="allergies4" placeholder="Allergy" class="form-control"></td>
        <td><input type="text" name="status4"  placeholder="Description" class="form-control"></td>
      </tr>
      <tr>
      <td>  <input type="button" id="aller4" value="ADD MORE" class='btn btn-primary ficha'></td>
      </tr>
            <tr id="allerge5" class="ficha">
        <td><input type="text" name="allergies5" placeholder="Allergy" class="form-control"></td>
        <td><input type="text" name="status5"  placeholder="Description" class="form-control"></td>
      </tr>
    </table>

</div>



</div>
</div>
<div id="tab-5" class="tab-pane">
<div class="panel-body">
<div class="col-md-6 b-r">
<div class="form-group">
  <label class="control-label">Bowel Habits ?</label>
  <div class="">
    <label class="checkbox-inline"> <input type="radio" class="bowels"  name="bowel" value="Normal" > Normal</label>
    <label class="checkbox-inline"> <input type="radio" class="bowels"  name="bowel" value="Abnormal"> Abnormal</label>
  </div>
</div>
<div class="form-group bowelexp ficha">
  <label class="control-label">Details</label>
  <textarea class="form-control" rows="3"  name="bowel_details"> </textarea>
</div>

<div class="form-group">
  <label class="control-label">Urinary Habits ?</label>
  <div class="">
    <label class="checkbox-inline"> <input type="radio" class="urinarys"  name="urinary" value="Normal" > Normal</label>
    <label class="checkbox-inline"> <input type="radio" class="urinarys"  name="urinary" value="Abnormal"> Abnormal</label>
  </div>
</div>
<div class="form-group urinaryexp ficha">
  <label class="control-label">Details</label>
  <textarea class="form-control" rows="3"  name="urinary_details"> </textarea>
</div>

</div>
<div class="col-md-6">

<div class="form-group">
  <label class="control-label">Sleep Habits ?</label>
  <div class="">
    <label class="checkbox-inline"> <input type="radio" class="sleeps"  name="sleep" value="Normal" > Normal</label>
    <label class="checkbox-inline"> <input type="radio" class="sleeps"  name="sleep" value="Abnormal"> Abnormal</label>
  </div>
</div>
<div class="form-group sleepexp ficha">
  <label class="control-label">Details</label>
  <textarea class="form-control" rows="3"  name="sleep_details"> </textarea>
</div>

<div class="form-group">
  <label class="control-label">Appetite Habits ?</label>
  <div class="">
    <label class="checkbox-inline"> <input type="radio" class="appetites"  name="appetite" value="Normal" > Normal</label>
    <label class="checkbox-inline"> <input type="radio" class="appetites"  name="appetite" value="Abnormal"> Abnormal</label>
  </div>
</div>
<div class="form-group appetiteexp ficha">
  <label class="control-label">Details</label>
  <textarea class="form-control" rows="3"  name="appetite_details"> </textarea>
</div>
</div>

</div>
</div>
<div id="tab-6" class="tab-pane">
<div class="panel-body">
  <div class="form-group">
    <label class="control-label">Family History </label><br>
      <div class="col-lg-10">
      <textarea class="form-control" rows="5"  name="fam_note">@if($fam){{$fam->notes}}@endif</textarea>
    </div>
  </div>
</div>
</div>



<div class="col-md-10 col-md-offset-2">
<button class="btn btn-sm btn-primary btn-rounded btn-block" type="submit"><strong>@if($cmed || $psummary || $fam)UPDATE @else SUBMIT @endif </strong></button>
</div>
{{ Form::close() }}
          </div>

      </div>

  </div>
</div>
</div>















@endsection
@section('script-test')
<script>
$(document).ready(function() {

  $(".bowels").click(function(){
  if($('input[name=bowel]:checked').val()=='Abnormal'){
    $(".bowelexp").show();
  }else{
    $(".bowelexp").hide();
  }
});


$(".appetites").click(function(){
if($('input[name=appetite]:checked').val()=='Abnormal'){
  $(".appetiteexp").show();
}else{
  $(".appetiteexp").hide();
}
});

$(".sleeps").click(function(){
if($('input[name=sleep]:checked').val()=='Abnormal'){
  $(".sleepexp").show();
}else{
  $(".sleepexp").hide();
}
});

$(".urinarys").click(function(){
if($('input[name=urinary]:checked').val()=='Abnormal'){
  $(".urinaryexp").show();
}else{
  $(".urinaryexp").hide();
}
});


});
</script>
<script type="text/javascript">
$("#aller1").click(function(){
    $("#allerge2").show();
    $("#aller1").hide();
    $("#aller2").show();
});

$("#aller2").click(function(){
    $("#allerge3").show();
    $("#aller2").hide();
    $("#aller3").show();
});

$("#aller3").click(function(){
    $("#allerge4").show();
    $("#aller3").hide();
    $("#aller4").show();
});

$("#aller4").click(function(){
    $("#allerge5").show();
    $("#aller4").hide();
});
</script>

<script type="text/javascript">
$("#cond2").click(function(){
    $("#con2").show();
    $("#cond2").hide();
    $("#cond3").show();

});

$("#cond3").click(function(){
  $("#con3").show();
  $("#cond3").hide();
  $("#cond4").show();
});

$("#cond4").click(function(){
  $("#con4").show();
  $("#cond4").hide();
  $("#cond5").show();
});

$("#cond5").click(function(){
  $("#con5").show();
  $("#cond5").hide();

});
</script>
@endsection
