@extends('layouts.doctor_layout')
@section('title', 'Quick Diagnosis')
@section('content')


<?php



           $stat= $pdetails->status;
         $afyauserId= $pdetails->afya_user_id;
          $dependantId= $pdetails->persontreated;
          $app_id_prev= $pdetails->last_app_id;
          $app_id= $pdetails->id;
          $doc_id= $pdetails->doc_id;
          $fac_id= $pdetails->facility_id;
          $fac_setup= $pdetails->set_up;
          $dependantAge = $pdetails->depdob;
          $AfyaUserAge = $pdetails->dob;
        $condition = $pdetails->condition;
?>

@section('leftmenu')
@include('includes.doc_inc.leftmenu2')
@endsection
@include('includes.doc_inc.topnavbar_v2')


<!--tabs Menus-->

<div class="row wrapper border-bottom  page-heading">

          <?php $condy=DB::table('patient_diagnosis')
          ->Where('appointment_id',$app_id)
         ->select('patient_diagnosis.id','patient_diagnosis.disease_id as name','patient_diagnosis.date_diagnosed')
         ->get();
          ?>

   <div class="col-lg-6">
     <div class="ibox float-e-margins">
       <div class="ibox-title">
         <h5>Patient Diagnosis</h5>
       </div>
       <div class="ibox-content">
         {{ Form::open(array('route' => array('quickdiag'),'method'=>'POST' ,'class'=>'form-horizontal')) }}

         <div class="form-group ">
         <label  class="col-lg-4 control-label">Condition:</label>
         <div class="col-lg-6">
           <input type="text" class="form-control" value="" name="disease"  >

           <select id="diseases" name="disease" class="form-control d_list2" style="width: 100%"></select> 
         </div>
         </div>

         <div class="form-group">
         <label class="col-lg-4 control-label">Type of Diagnosis:</label>
         <div class="col-lg-6"><select class="form-control" name="level"  style="width: 100%" >
         <option value=''>Choose one</option>
         <option value='Primary'>Primary</option>
         <option value='Secondary'>Secondary</option>
         </select>
         </div></div>

         <div class="form-group">
         <label class="col-lg-4 control-label">Chronic:</label>
         <div class="col-lg-6"><select class="form-control" name="chronic"  style="width: 100%" >
         <option value=''>Choose one</option>
         <option value='Y'>YES</option>
         <option value='N'>No</option>
         </select>
         </div></div>
         <div class="form-group">
         <label class="col-lg-4 control-label">Level of Severity:</label>
         <div class="col-lg-6"><select class="form-control" name="severity"  style="width: 100%" >
         <?php $severeity=DB::table('severity')->get();
         ?>
         <option value=''>Choose one</option>
         @foreach($severeity as $diag)
         <option value='{{$diag->id}}'>{{$diag->name}}</option>
         @endforeach
         </select>
         </div></div>

         <div class="form-group">
         <label class="col-lg-4 control-label">Supportive Care:</label>
         <div class="col-lg-6"><select class="form-control" name="care"  style="width: 100%" >
         <?php $scare=DB::table('supportive_care')->get();
         ?>
         <option value=''>Choose one</option>
         @foreach($scare as $sup)
         <option value='{{$sup->name}}'>{{$sup->name}}</option>
         @endforeach
         </select>
         </div>
         </div>
         {{ Form::hidden('state','Normal', array('class' => 'form-control')) }}
         {{ Form::hidden('appointment_id',$app_id, array('class' => 'form-control')) }}
         {{ Form::hidden('afya_user_id',$afyauserId, array('class' => 'form-control')) }}

         <div class="col-lg-offset-5">
         <button class="btn btn-sm btn-primary  m-t-n-xs" type="submit"><strong>Submit</strong></button>
         </div>
         {{ Form::close() }}
       </div>
     </div>
   </div>



   <div class="col-lg-6">
     <div class="ibox float-e-margins">
       <div class="ibox-title">
         <h5>Diagnosis Made</h5>

       </div>
       <div class="ibox-content">
         <ul class="todo-list m-t small-list">
           @foreach($condy as $diags)
           <li class="list-group-item"><span class="badge">{{$diags->date_diagnosed}}</span>{{$diags->name}}</li>
           @endforeach


         </ul>
       </div>
     </div>
   </div>



</div><!-- row -->





@endsection
