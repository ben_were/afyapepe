@extends('layouts.doctor_layout')
@section('title', 'Patient History')
@section('content')



<?php
use Carbon\Carbon;
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){
$Did = $Docdata->id;
$Name = $Docdata->name;

}
$afyauserId = $user->afyaId;
$apps = DB::table('appointments')
->orderBy('id', 'desc')
->where('afya_user_id', '=',$afyauserId)->first();
$app_id = $apps->id;


$condition = $user->condition;
if($user->persontreated == 'Self'){
  $name = $user->firstname ." ". $user->secondName;
  $gender = $user->gender;
  $dob = $user->dob;
  $age = $user->age;
  $occupation = $user->occupation;
}
if($dob){
$interval = date_diff(date_create(), date_create($dob));
$age= $interval->format(" %Y Years Old");
}else{
$age=$age;
}
$date1=$ptdetails->created_at;

// ('l jS \\of F Y h:i:s A');

$timestamp = strtotime($ptdetails->created_at);
$date1= date("jS", $timestamp);
$date2= date("D F y", $timestamp);

$height1 =$ptdetails->current_height;
if($height1){
$height =$height1/100;
$BMI2 =$ptdetails->current_weight/($height*$height);
$BMI =number_format((float)$BMI2, 2, '.', '');
}else{
  $BMI ='';
}

$timestampt1 = strtotime($doct->created_at);
$timestampt2 = strtotime($doct->updated_at);
$Time1= date("h:i:s A", $timestampt1);

$intervalT = date_diff(date_create($doct->updated_at), date_create($doct->created_at));
$Time2= $intervalT->format("%h Hour :%i mins");


// $Time2= date("h:i:s A", $timestampt2);
?>
@section('leftmenu')
@include('includes.doc_inc.leftmenu')
@endsection

<div class="row wrapper white-bg page-heading">
              <div class="col-lg-6">
                  <h2>PATIENT</h2>
                  <ol class="breadcrumb">
                      <li>
                          <a href="#">  <strong>Name:</strong> {{$name}}</a>
                      </li>
                      <li>
                          <strong>Gender:</strong> {{$gender}}<br>

                      </li>
                      <li class="active">
                          <strong>Age:</strong> {{$age}}
                      </li>
                  </ol>
              </div>
              <div class="col-lg-6">
                  <div class="title-action">

                      <a href="{{url('doctor.patient_history',$user->appid)}}"  class="btn btn-primary"><i class="fa fa-angle-double-left"></i> BACK </a>
                      <a href="{{url('doctor.edithistory2',$user->appid)}}"  class="btn btn-primary"><i class="fa fa-edit"></i> EDIT </a>
            <input name="b_print" type="button" class="btn btn-primary"   onClick="printdiv('div_print');" value="PRINT">
            <a href="{{url('doctor.slideshow',$afyauserId)}}"  class="btn btn-primary"><i class="fa fa-edit"></i> SLIDE SHOW </a>

                  </div>
              </div>
          </div>





<div class="wrapper wrapper-content">
  <div class="row"  id="div_print">

    <div class="col-lg-10">
      <div class="ibox float-e-margins">
        <div class="ibox-content">
             <table class="table table-bordered">
<tr> <td width="10%"><h3> {{$date1}}</h3> {{$date2}}</td>
  <td colspan="2" width="40%">{{$doct->name}}  <br> {{$doct->FacilityName}} <br>At {{$Time1}} for {{$Time2}} </td>
  <td  width="50%"> <p class="pull-right">{{$name}} <br> {{$gender}} <br> {{$age}}</p></td>
</tr>

  <tr>
    <td colspan="2">TRIAGE</td><td colspan="2">
<div class="col-xs-3">
<small class="stats-label">Height </small>
<strong> {{$ptdetails->current_height}}cm</strong>
</div>
<div class="col-xs-3">
<small class="stats-label">Weight </small>
<strong> {{$ptdetails->current_weight}}kg</strong>
</div>
<div class="col-xs-3">
<small class="stats-label">Temperature </small>
<strong> {{$ptdetails->temperature}}°C</strong>
</div>
<div class="col-xs-3">
<small class="stats-label">BMI </small>
<strong>@if($BMI){{$BMI}}@endif</strong>
</div>

<div class="col-xs-3">
<small class="stats-label">HR </small>
<strong> {{$ptdetails->hr}}b/min</strong>
</div>

<div class="col-xs-3">
<small class="stats-label">BP</small>
<strong> @if($ptdetails->systolic_bp){{$ptdetails->systolic_bp}} / {{$ptdetails->diastolic_bp}}mmHg @endif</strong>
</div>

<div class="col-xs-3">
<small class="stats-label">SpO2</small>
<strong> {{$ptdetails->rr}}breaths/min</strong>
</div>

<div class="col-xs-3">
<small class="stats-label">RBS </small>
<strong> {{$ptdetails->rbs}}mmol/l </strong>
</div>
    </td>
  </tr>
@if($summary)
<tr>
 <td colspan="2">PATIENT SUMMARY</td><td colspan="2">
 @foreach($summary as $summ)
     {{$summ->notes}} <br>
  @endforeach</td>
</tr>
@endif
@if($cmeds)
<tr>
<td colspan="2">CURRENT MEDICATION</td><td colspan="2">
  @foreach($cmeds as $cmed)
      {{$cmed->drugs}} <br>
   @endforeach</td>
</tr>
@endif
@if($ge)

<tr>
<td colspan="2">EXAMINATION FINDINGS</td>
<td colspan="2">
  <div class="col-xs-3">
  <small class="stats-label">GENERAL EXAMINATION</small>
  <strong>{{$ge->g_examination}}</strong>
  </div>
  <div class="col-xs-3">
  <small class="stats-label">CVS</small><br>
  <strong>{{$ge->cvs}}</strong>
  </div>
  <div class="col-xs-3">
  <small class="stats-label">RS</small><br>
  <strong>{{$ge->rs}}</strong>
  </div>
  <div class="col-xs-3">
  <small class="stats-label">PA</small><br>
  <strong>{{$ge->pa}}</strong>
  </div>
  <div class="col-xs-3">
  <small class="stats-label">CNS</small><br>
  <strong>{{$ge->cns}}</strong>
  </div>
  <div class="col-xs-3">
  <small class="stats-label">MSS</small><br>
  <strong>{{$ge->mss}}</strong>
  </div>
  <div class="col-xs-3">
  <small class="stats-label">PERIPHERIES</small><br>
  <strong>{{$ge->peripheries}}</strong>
  </div>
</td>
</tr>
@endif
@if($diagnosis)
<tr>
<td colspan="2">DIAGNOSIS</td><td colspan="2">
   @foreach($diagnosis as $tst1)
  {{$tst1->disease_id}} <br>
  @endforeach</td>
</tr> @endif

@if($tsts)
<tr>
<td colspan="2">TESTS</td><td colspan="2">
@foreach($tsts as $tst)
{{$tst->tname}}  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  {{$tst->value}}</br>
@endforeach
</td>
<tr>
@endif
  @if($rady)
<td colspan="2">TESTS</td><td colspan="2">

                              @foreach($rady as $radst)
                              <?php
                              $test = '';
                              $result= '';
                              if ($radst->test_cat_id== '9') {
                                $ct=DB::table('ct_scan')->where('id', '=',$radst->test) ->first();
                                $ctresult=DB::table('radiology_test_result')->where('radiology_td_id', '=',$radst->patTdid) ->first();
                                $test = $ct->name;
                              if($ctresult){  $result = $ctresult->results; }
                              } elseif ($radst->test_cat_id== 10) {
                                $xray=DB::table('xray')->where('id', '=',$radst->test) ->first();
                                $xrayresult=DB::table('radiology_test_result')->where('radiology_td_id', '=',$radst->patTdid) ->first();
                                $test = $xray->name;
                              if($xrayresult){  $result = $xrayresult->results; }
                              } elseif ($radst->test_cat_id== 11) {
                                $mri=DB::table('mri_tests')->where('id', '=',$radst->test)->first();
                                $mriresult=DB::table('radiology_test_result')->where('radiology_td_id', '=',$radst->patTdid) ->first();
                                $test = $mri->name;
                          if($mriresult)  { $result = $mriresult->results; }
                              }elseif ($radst->test_cat_id== 12) {
                                $ultra=DB::table('ultrasound')->where('id', '=',$radst->test) ->first();
                                $ultraresult=DB::table('radiology_test_result')->where('radiology_td_id', '=',$radst->patTdid) ->first();
                                $test = $ultra->name;
                              if($ultraresult) { $result = $ultraresult->results;  }
                              }elseif ($radst->test_cat_id== 13) {
                                $other=DB::table('other_tests')->where('id', '=',$radst->test) ->first();
                                $otherresult=DB::table('radiology_test_result')->where('radiology_td_id', '=',$radst->patTdid) ->first();
                                $test = $other->name;
                              if($otherresult) { $result = $otherresult->results;}
                              }


                              ?>
{{$test}}   @if($result) <strong>&nbsp;&nbsp;&nbsp;&nbsp; RESULTS &nbsp;&nbsp;&nbsp;&nbsp;</strong>  {{$result}} @endif</br>
@endforeach
</td>
</tr>
@endif
@if($procedures)
<tr>
<td colspan="2">Procedures :</td><td colspan="2">
@foreach($procedures as $prc)
{{$prc->description}} <br>
@endforeach</td>
</tr>
@endif
@if($prescriptions)
<tr>
<td colspan="2">Prescriptions :</td><td colspan="2">
@foreach($prescriptions as $prsc)
{{$prsc->drug_id}}<br>
@endforeach</td>
</tr>
@endif
</table>

        </div>
      </div>
    </div>


  </div>

</div>

    @endsection
    @section('script-test')


    @endsection
