@extends('layouts.registrar_layout')
@section('title', 'Registrar Dashboard')
@section('content')
<?php
$facility = DB::table('facility_registrar')
->join('facilities', 'facilities.FacilityCode', '=', 'facility_registrar.facilitycode')
->select('facility_registrar.facilitycode','facilities.set_up')
->where('facility_registrar.user_id', Auth::user()->id)
->first();
$facilitycode = $facility->facilitycode;
$setup = $facility->set_up;

?>
<div class="wrapper wrapper-content animated fadeInRight">
      <div class="row">
          <div class="col-lg-12">
              <div class="text-center m-t-lg">
                  <h1>
                      Person to be treated
                  </h1>
                  <!-- <small>Adult as Self or Minor.</small> -->
              </div>
          </div>
      </div>
  </div>
<div class="wrapper wrapper-content animated fadeInRight">
  <br><br><br><br><br><br>

   @if(!empty($facilitycode) && $setup == 'Partial')
  <div class="row">
            <div class="col-sm-5 col-sm-offset-1">

               <a href="{{URL('registrar.shows',$id)}}" class="btn btn-primary btn-block">{{'Primary'}}</a>

            </div>
            <div class="col-sm-5">

                  <a href="{{URL('registrar.showdependants',$id)}}" class="btn btn-success btn-block">{{'Dependant'}}</a>

            </div>
          </div>
        @elseif(!empty($facilitycode) && $setup != 'Partial')
        <div class="row">

            <div class="col-sm-5 col-sm-offset-1">

               <a href="{{URL('registrar.show',$id)}}" class="btn btn-primary btn-block">{{'Primary'}}</a>

            </div>
            <div class="col-sm-5">

                  <a href="{{URL('registrar.dependants',$id)}}" class="btn btn-success btn-block">{{'Dependant'}}</a>

            </div>
          </div>
          @endif
        </div>

     @include('includes.default.footer')
@endsection
