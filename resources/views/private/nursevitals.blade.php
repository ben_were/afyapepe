@extends('layouts.doctor_layout')
@section('title', 'Dashboard')

@section('content')
<?php
$doc = (new \App\Http\Controllers\DoctorController);
$Docdatas = $doc->DocDetails();
foreach($Docdatas as $Docdata){
$Did = $Docdata->id;
$Name = $Docdata->name;
$Address = $Docdata->address;
$RegNo = $Docdata->regno;
$RegDate = $Docdata->regdate;
$Speciality = $Docdata->speciality;
$Sub_Speciality = $Docdata->subspeciality;
$Duser = $Docdata->user_id;
}

$app_id=$db->appid;
$gender=$db->gender;
$afyaId=$db->afyaId;
$condition = $db->condition;
$afyauserId =$db->id;


 ?>

 @section('leftmenu')
 @include('includes.doc_inc.leftmenu2')
 @endsection

 @include('includes.doc_inc.topnavbar_v2')
  @include('doctor.triage')

<!-- <div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>Triage form <small>Add Patient Triage</small></h5>
                <div class="ibox-tools">

                </div>
            </div>
            <div class="ibox-content">
                <div class="row">
                    <div class="col-sm-3 b-r"><h3 class="m-t-none m-b"></h3>
                        {!! Form::open(array('url' => 'private.createdetail','method'=>'POST')) !!}


                           <div class="form-group">
                           <label for="exampleInputEmail1">Weight (kg)</label>
                           <input type="name" class="form-control"  placeholder="e.g ..80" name="weight"  >
                           <input type="hidden" value="{{$afyaId}}" name="id">
                           <input type="hidden" value="{{$app_id}}" name="appointment">
                           </div>

                           <div class="form-group">
                           <label for="exampleInputEmail1">Height (cm)</label>
                           <input type="name" class="form-control" placeholder="e.g ..180" name="current_height" >
                           </div>
       </div>
         <div class="col-sm-3 b-r">
           <br>
                          <div class="form-group">
                           <label for="exampleInputPassword1">Temperature (°C)</label>
                           <input type="name" class="form-control"  placeholder="e.g .. 37.2" name="temperature"  >
                          </div>

                           <div class="form-group">
                           <label for="exampleInputPassword1">Systolic BP</label>
                           <input type="name" class="form-control"  placeholder="Systolic BP" name="systolic">
                           </div>
</div>
  <div class="col-sm-3 b-r"><h3 class="m-t-none m-b"></h3>
                           <div class="form-group">
                           <label for="exampleInputEmail1">Diastolic BP</label>
                           <input type="name" class="form-control" aria-describedby="emailHelp" placeholder="Diastolic BP" name="diastolic" >
                           </div>

                           <div class="form-group">
                           <label for="exampleInputEmail1">RBS mmol/l</label>
                           <input type="name" class="form-control"  aria-describedby="emailHelp" placeholder="RBS mmol/l" name="rbs">
                           </div>



</div>
<div class="col-sm-3"><h4></h4>
  <div class="form-group">
  <label for="exampleInputEmail1">HR b/min</label>
  <input type="name" class="form-control"  aria-describedby="emailHelp" placeholder="HR b/min" name="hr">
  </div>

  <div class="form-group">
  <label for="exampleInputEmail1">SpO<small>2</small></label>
  <input type="name" class="form-control"  aria-describedby="emailHelp" placeholder="SpO2" name="rr">
  </div>



                          @if($gender=='Female')

                          <div class="form-group">
                          <label for="exampleInputPassword1">Pregnant?</label>
                          <input type="radio" value="No"  name="pregnant"> No <input type="radio" value="Yes"  name="pregnant"> Yes
                          </div>
                          <div class="form-group" id="data_1">
                      <label for="exampleInputPassword1">LMP</label><br />
                        <div class="input-group date">
                          <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                           <input type="text" class="form-control" name="lmp">
                                      </div>
                               </div>
                         @endif
                         <div class="pull-right">
                           <button type="submit" class="btn btn-primary">Submit</button>
                         </div>
                           {!! Form::close() !!}
    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

  </div> -->














@endsection
